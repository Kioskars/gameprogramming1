#pragma once
#include "ISubSystem.h"

class Engine;
class Keyboard;
class Mouse;
class GameStatesMachine;


class InputManager : public ISubSystem
{
public:
	InputManager(Engine* p_pxEngine, GameStatesMachine* p_pxGameStateMachine);
	~InputManager();

	int Initialize();
	int Shutdown();

	void HandleEvents();
	Mouse* GetMouse();
	Keyboard* GetKeyboard();
	
	bool IsMouseButtonDown(int p_iIndex) { return false; }
	bool IsKeyDown(int p_iIndex) { return false; }
	
	int GetMouseX();
	int GetMouseY();

private:
	Keyboard* m_pxKeyboard;
	Mouse* m_pxMouse;
	Engine* m_pxEngine;
	GameStatesMachine* m_pxGameStateMachine;

};