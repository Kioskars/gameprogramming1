#include "stdafx.h"
#include "PlayState.h"
#include "Froggy.h"
#include "GraphicsCore.h"
#include "SpriteManager.h"
#include "Sprite.h"
#include "Car.h"
#include "CollisionManager.h"
#include "GameStatesMachine.h"
#include "MenuState.h"
#include "EntityManager.h"
#include "SoundManager.h"
#include "Sound.h"
#include "Music.h"
#include "HUDmanager.h"
#include "EndState.h"
#include "IEntity.h"

PlayState::PlayState(System & p_xSystem)
{
	SetGameStateID(PLAY_STATE);
	m_xSystem = p_xSystem;
	m_xCar = nullptr;
	m_xCar2 = nullptr;
	m_xCar3 = nullptr;
	m_xCar4 = nullptr;
	m_xFroggy = nullptr;
}

PlayState::~PlayState()
{
	OnExit();
}

bool PlayState::Update(float p_fDeltaTime)
{
	m_xSystem.m_pxGraphicsCore->GetHUDmanager()->UpdateTimeCounter(p_fDeltaTime);
	for (auto it = m_apxGameObjects.begin(); it != m_apxGameObjects.end(); ++it)
	{
		(*it)->Update(p_fDeltaTime);
	}
	FroggyCollidedAndLostALife(m_xCar);
	FroggyCollidedAndLostALife(m_xCar2);
	FroggyCollidedAndLostALife(m_xCar3);
	FroggyCollidedAndLostALife(m_xCar4);
	

	if (FroggyWin(m_xFroggy))
	{
		m_xSystem.m_bFroggyWin = true;
		m_xSystem.m_pxGameStatesMachine->ChangeState(new EndState(m_xSystem));
		return true;
	}

	if (m_xFroggy->GetLives() <= 0 || m_xSystem.m_pxGraphicsCore->GetHUDmanager()->GetTimeleft() <= 0)
		m_xSystem.m_pxGameStatesMachine->ChangeState(new EndState(m_xSystem));
	
	return true;
}

void PlayState::Draw()
{
	m_xSystem.m_pxGraphicsCore->GetHUDmanager()->DrawGameInstructions();
	for (auto it = m_apxGameObjects.begin(); it != m_apxGameObjects.end(); ++it)
	{
		m_xSystem.m_pxGraphicsCore->Draw((*it)->GetSprite(), (*it)->GetX(), (*it)->GetY(), m_xSystem.m_fGfxScale);
	}
	m_xSystem.m_pxGraphicsCore->GetHUDmanager()->DrawPlayerLives(m_xFroggy->GetLives(), m_xSystem.m_iScreenWidth - 40, 10);
	m_xSystem.m_pxGraphicsCore->GetHUDmanager()->TimeCounter(m_xSystem.m_iScreenWidth - 320, m_xSystem.m_iScreenHeight - 30);
}

/*
* Sets up the PlayState. Creating all of the objects sprites and sounds. 
* Also puts the objects in the EntityManagers vector containers.
*/
bool PlayState::OnEnter()
{
	
	m_xFroggy = m_xSystem.m_pxEntityManager->CreateFroggy();
	m_xCar = m_xSystem.m_pxEntityManager->CreateCar();
	m_xCar2 = m_xSystem.m_pxEntityManager->CreateCar();
	m_xCar3 = m_xSystem.m_pxEntityManager->CreateCar();
	m_xCar4 = m_xSystem.m_pxEntityManager->CreateCar();

	m_apxGameObjects.push_back(m_xCar4);
	m_apxGameObjects.push_back(m_xCar3);
	m_apxGameObjects.push_back(m_xCar2);
	m_apxGameObjects.push_back(m_xCar);
	m_apxGameObjects.push_back(m_xFroggy);

	m_xCar->SetXandY(-100, 50);
	m_xCar2->SetXandY(-230, 250);
	m_xCar3->SetXandY(-50, 350);
	m_xCar4->SetXandY(-50, 450);

	m_xCar->SetSpeed(800);
	m_xCar2->SetSpeed(600);
	m_xCar2->SetSpeed(350);
	m_xCar4->SetSpeed(450);

	m_pxFroggyCollideSound = m_xSystem.m_pxSoundManager->CreateSound("../Assets/explosion-01.wav");

	m_xSystem.m_pxGraphicsCore->GetHUDmanager()->CreatePlayerLives(m_xFroggy->GetLives(), m_xSystem.m_iScreenWidth - 40, 10);
	m_xSystem.m_pxGraphicsCore->GetHUDmanager()->SetTimeLeft(152);
	m_xSystem.m_pxGraphicsCore->GetHUDmanager()->CreateGameInstructions();
	m_xSystem.m_pxSoundManager->PrintAllSoundsInfo();
	
	printf("Created Froggy at pos: %f %f\n", m_xFroggy->GetX(), m_xFroggy->GetY());
	printf("Created Car at pos: %f %f\n", m_xCar->GetX(), m_xCar->GetY());
	printf("Created Car2 at pos: %f %f\n", m_xCar2->GetX(), m_xCar2->GetY());
	printf("Created Car3 at pos: %f %f\n", m_xCar3->GetX(), m_xCar2->GetY());
	printf("Created Car4 at pos: %f %f\n", m_xCar4->GetX(), m_xCar2->GetY());
	
	return true;
}

/*
* Dallocates everything from PlayState.
*/
bool PlayState::OnExit()
{
	printf("Exiting state: %i\n", (int)GetGameStateID());
	
	m_xSystem.m_pxEntityManager->DeallocEntitiesList();
	m_xSystem.m_pxGraphicsCore->GetHUDmanager()->DeAllocLives();
	m_xSystem.m_pxGraphicsCore->GetHUDmanager()->DeAllocGameInstructions();

	return true;
}

System & PlayState::GetSystem()
{
	return m_xSystem;
}

void PlayState::FroggyCollidedAndLostALife(Car* p_xCar)
{
	int iOverlapX = 0;
	int	iOverlapY = 0;

	//Checks whether froggy collided with an selected car.
	if (CollisionManager::Check(m_xFroggy->GetCollider(),
		p_xCar->GetCollider(),
		iOverlapX, iOverlapY))
	{
		printf("Froggy collided with Car\n");
		m_xFroggy->LoseLife();
		m_xFroggy->SetX(m_xSystem.m_iScreenWidth / 2 - m_xFroggy->GetSprite()->GetRegion()->w*m_xSystem.m_fGfxScale / 2);
		m_xFroggy->SetY(m_xSystem.m_iScreenHeight - m_xFroggy->GetSprite()->GetRegion()->h*m_xSystem.m_fGfxScale - 50);
		m_pxFroggyCollideSound->PlaySound();
		m_xSystem.m_pxGraphicsCore->GetHUDmanager()->DestroyLife();
		if (m_xFroggy->GetLives() <= 0 )
			printf("Froggy is now dead...player lost...");
	}
}

//Checks if froggy objects Y position is less than 5 pixels from top.
bool PlayState::FroggyWin(Froggy* p_xFroggy)
{
	if(p_xFroggy != NULL)
	{ 
		if (p_xFroggy->GetY() <= 5)
		{
			printf("Froggy WON!\n");
			return true;
		}
		return false;
	}
	return false;
}
