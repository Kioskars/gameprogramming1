#pragma once
#include "IGameStates.h"

class GameStatesMachine;
class Music;
class Sprite;

class MenuState : public IGameStates
{
public:
	MenuState(System& xSystem);
	~MenuState();

	bool Update(float p_fDeltaTime);
	void Draw();

	bool OnEnter();
	bool OnExit();

	System& GetSystem();
private:
	System m_xSystem;
	Music* m_pxGameTheme;
	Music* m_pxMusic2;
	Sprite* m_pxMainMenu;
	
};