#pragma once
#include "IGameStates.h"

class Sprite;
class Music;
class SoundManager;

class EndState : public IGameStates
{
public:
	EndState(System& p_xSystem);
	~EndState();

	bool OnEnter();
	bool Update(float p_fDeltaTime);
	bool OnExit();
	void Draw();

	System& GetSystem();
private:
	System m_xSystem;
	Sprite* m_pxEndScreen;
	Sprite* m_pxNewGame;
	bool m_bFroggyWin;
};