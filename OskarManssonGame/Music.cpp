#include "stdafx.h"
#include "Music.h"

Music::Music(const std::string p_sFilepath)
{
	//When creating a Music type it will automatically call the SDL_mixer function Mix_LoadMUS
	//Which will give us playable music data chunk.
	m_pxMusicChunk = Mix_LoadMUS(p_sFilepath.c_str());
	if (!m_pxMusicChunk)
	{
		printf("Mix_LoadMUS(p_sFilepath.c_str()): %s\n", Mix_GetError());
	}
	m_sMusicName = p_sFilepath.c_str();
}

Music::~Music()
{
}

void Music::PlayMusic()
{
	printf("Playing...%s\n", m_sMusicName.c_str());
	if (Mix_PlayMusic(m_pxMusicChunk, 0) == -1)
	{
		printf("Mix_Music: %s\n", Mix_GetError());
	}
}

void Music::PauseMusic()
{
	Mix_PauseMusic();
}

std::string Music::GetMusicName()
{
	return m_sMusicName;
}

Mix_Music * Music::GetMusicChunk()
{
	return m_pxMusicChunk;
}

void Music::SetMusic(Mix_Music * p_pxMusicChunk)
{
	m_pxMusicChunk = p_pxMusicChunk;
}
