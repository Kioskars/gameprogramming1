#pragma once
#include "IEntity.h"
class Keyboard;

class Car : public IEntity
{
public:
	Car(Keyboard* p_pxKeyboard, Sprite* p_pxSprite,
		float p_fX, float p_fY, float p_fSpeed, float p_fGfxScale,
		int p_iScreenWidth, int p_iScreenHeight);
	Car() {};
	~Car();
	void Update(float p_fDeltaTime);
	void Draw(Sprite* p_pxSprite, int p_iX, int p_iY, float p_fScale);
	Sprite* GetSprite();
	Collider* GetCollider();
	float GetX();
	float GetY();
	void SetXandY(float p_fX, float p_fY);
	void SetSpeed(float p_fSpeed);
	void SetGfxScale(float p_fGfxScale);
	
	bool IsVisible();
	void Movement(float p_fDeltaTime);
	EENTITYTYPE GetType();
private:
	
	Sprite* m_pxSprite;
	Collider* m_pxCollider;
	Keyboard* m_pxKeyboard;
	float m_fX;
	float m_fY;
	float m_fSpeed;
	float m_fGfxScale;
	int m_iScreenWidth;
	int m_iScreenHeight;
	bool m_bVisible;

};