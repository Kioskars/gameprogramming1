#pragma once

#include "stdafx.h"

class ISubSystem
{
public:
	virtual int Initialize() = 0;
	virtual int Shutdown() = 0;
	
	virtual int GetID() { return m_iSubSystemID; }
	virtual std::string GetSubSystemIDName() { return m_sSubSystemIDName; }

	virtual void SetID(int p_ID) { this->m_iSubSystemID = p_ID; }
	virtual void SetSubSystemIDName(std::string p_sIDName) { this->m_sSubSystemIDName = p_sIDName; }

protected:
	int m_iSubSystemID = NULL;
	std::string m_sSubSystemIDName;
};