#pragma once

class Music
{
public:
	Music(const std::string p_sFilepath);
	~Music();

	void PlayMusic();
	void PauseMusic();

	std::string GetMusicName();

	Mix_Music* GetMusicChunk();
	void SetMusic(Mix_Music* p_pxMusicChunk);
private:
	Mix_Music* m_pxMusicChunk;
	int m_iLastChannelPlayed;
	std::string m_sMusicName;
};