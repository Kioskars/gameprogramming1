#pragma once

#include "ISubSystem.h"
#include "IEntity.h"
#include "IGameStates.h"

class SpriteManager;
class Froggy;
class Car;
class IGamestates;
class IEntity;

enum EENTITYTYPE;


class EntityManager : public ISubSystem
{
public:
	EntityManager(SpriteManager* p_pxSpriteManager);
	EntityManager();
	~EntityManager();

	int Initialize();
	int Shutdown();

	Froggy* CreateFroggy();
	Car* CreateCar();

	void AssignSystems(System p_xSystems);
	
	void DeallocCarsList();
	void DeallocFroggyList();
	void DeallocEntitiesList();
	
private:
	SpriteManager* m_pxSpriteManager;
	System m_xSystem;
	std::vector<Car*> m_pxCarsList;
	std::vector<Froggy*> m_pxFroggysList;
	std::vector<IEntity*> m_pxEntitiesList;
};