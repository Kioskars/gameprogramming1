#pragma once

class GraphicsCore;
class CollisionManager;
class SpriteManager;
class Keyboard;
class GameStatesMachine;
class EntityManager;
class SoundManager;

struct System
{
	int m_iScreenWidth;
	int m_iScreenHeight;
	float m_fGfxScale;
	SpriteManager* m_pxSpriteManager;
	GraphicsCore* m_pxGraphicsCore;
	Keyboard* m_pxKeyboard;
	CollisionManager* m_pxCollisionManager;
	GameStatesMachine* m_pxGameStatesMachine;
	EntityManager* m_pxEntityManager;
	SoundManager* m_pxSoundManager;
	bool m_bFroggyWin;
};

enum GameStateID
{
	MENU_STATE = 0,
	PLAY_STATE = 1,
	END_STATE = 2,
};

class IGameStates
{
public:
	virtual bool OnEnter() = 0;
	virtual bool Update(float p_fDeltaTime) = 0;
	virtual bool OnExit() = 0;
	virtual void Draw() = 0;

	GameStateID GetGameStateID() { return m_GameStateID; }
	void SetGameStateID(GameStateID p_GameStateID) { this->m_GameStateID = p_GameStateID; }
	virtual System& GetSystem() = 0;
private:
	GameStateID m_GameStateID;
};