#include "stdafx.h"
#include "SoundManager.h"
#include "Music.h"
#include "Sound.h"

SoundManager::SoundManager()
{
	m_pxMusic = nullptr;
	m_pxSound = nullptr;
}

SoundManager::~SoundManager()
{
}

int SoundManager::Initialize()
{
	Mix_Init(MIX_INIT_MP3 | MIX_INIT_FLAC | MIX_INIT_OGG);
	if (Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 1, 1024) == -1)
	{
		const char* error = Mix_GetError();
		SDL_Log(error);
		return 0;
	}

	return 1;
}

/**
* Destroys and clears all of the containers elements.
*/
int SoundManager::Shutdown()
{
	for (auto it = m_apxSoundChunks.begin(); it != m_apxSoundChunks.end(); ++it)
	{
		printf("Destroying...%s\n", it->first.c_str());
		DestroySound(it->second);
	}
	for (auto it = m_apxMusicClips.begin(); it != m_apxMusicClips.end(); ++it)
	{
		printf("Destroying...%s\n", it->first.c_str());
		DestroyMusic(it->second);
	}
	m_apxMusicClips.clear();
	m_apxSoundChunks.clear();
	m_apxSoundClips.clear();
	return 1;
}

/*
* Creates sound data chunk from parameter.
*/
Sound * SoundManager::CreateSound(const std::string & p_sFilepath)
{
	//Check if there is any Sound files with the same path name in the map container m_apxSoundChunks.
	for (auto at = m_apxSoundChunks.begin(); at != m_apxSoundChunks.end(); ++at)
	{
		if (at->second->GetSoundName() == p_sFilepath.c_str())
		{
			printf("Sound Exist...returning sound\n");
			m_pxSound = at->second;
			return m_pxSound;
		}
	}

	//Check if there is any Sound files with the same path name in the vector container m_apxSoundclips.
	for (auto at = m_apxSoundClips.begin(); at != m_apxSoundClips.end(); ++at)
	{
		if ((*at)->GetSoundName() == p_sFilepath.c_str())
		{
			printf("Sound Exist...returning sound\n");
			m_pxSound = (*at);
			return m_pxSound;
		}
	}

	m_pxSound = new Sound(p_sFilepath);
	
	m_apxSoundChunks.insert(std::pair<std::string, Sound*>(p_sFilepath.c_str(), m_pxSound));
	m_apxSoundClips.push_back(m_pxSound);

	return m_apxSoundClips.back();
}

/*
* Creates music data chunk from parameter.
*/
Music * SoundManager::CreateMusic(const std::string & p_sFilepath)
{
	//Checks if ther is any file other file with same path name in map container m_apxMusicClips.
	for (auto at = m_apxMusicClips.begin(); at != m_apxMusicClips.end(); ++at)
	{
		if (at->second->GetMusicName() == p_sFilepath.c_str())
		{
			printf("Sound Exist...returning sound\n");
			m_pxMusic = at->second;
			return m_pxMusic;
		}
	}

	m_pxMusic = new Music(p_sFilepath);
	m_apxMusicClips.insert(std::pair<std::string, Music*>(p_sFilepath.c_str(), m_pxMusic));
	
	return m_pxMusic;
}

/**
* Takes the selected sound, frees and deletes it then resets the pointer to nullptr.
*/
void SoundManager::DestroySound(Sound * p_pxSound)
{
	Mix_FreeChunk(p_pxSound->GetSoundChunk());
	delete p_pxSound;
	p_pxSound = nullptr;
}

/**
* Takes the selected music, frees and deletes it then resets the pointer to nullptr.
*/
void SoundManager::DestroyMusic(Music * p_pxMusic)
{
	std::map<std::string, Music*>::iterator it = m_apxMusicClips.begin();
	
	Mix_FreeMusic(p_pxMusic->GetMusicChunk());
	p_pxMusic = nullptr;
}

/**
* Print what music and sound files are loaded into the SoundManagers containers.
*/
void SoundManager::PrintAllSoundsInfo()
{
	printf("All Music\n");
	for (auto it = m_apxMusicClips.begin(); it != m_apxMusicClips.end(); ++it)
		printf("Music: %s\n", it->second->GetMusicName().c_str());

	printf("All Sounds\n");
	for (auto it = m_apxSoundChunks.begin(); it != m_apxSoundChunks.end(); ++it)
		printf("Sound: %s\n", it->second->GetSoundName().c_str());

	//for (auto it = m_apxSoundClips.begin(); it != m_apxSoundClips.end(); ++it)
		//printf("Sound: %s\n", (*it)->GetSoundName().c_str());

}


