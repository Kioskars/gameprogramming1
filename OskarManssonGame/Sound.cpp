#include "stdafx.h"
#include "Sound.h"

Sound::Sound(std::string p_sFilepath)
{
	m_pxSoundClip = Mix_LoadWAV(p_sFilepath.c_str());
	if (!m_pxSoundClip)
	{
		printf("Mix_LoadWAV: %s\n", Mix_GetError());
	}

	this->m_sSoundName = p_sFilepath.c_str();
}

Sound::~Sound()
{
}

void Sound::PlaySound()
{
	printf("Played Sound%s\n", m_sSoundName.c_str());
	
	Mix_PlayChannel(-1, m_pxSoundClip, 0);
	
}

void Sound::StopSound()
{
	Mix_Pause(1);
}

std::string Sound::GetSoundName()
{
	return m_sSoundName;
}

Mix_Chunk * Sound::GetSoundChunk()
{
	return m_pxSoundClip;
}

void Sound::SetSoundClip(Mix_Chunk* p_pxMixChunk)
{
	m_pxSoundClip = p_pxMixChunk;
}
