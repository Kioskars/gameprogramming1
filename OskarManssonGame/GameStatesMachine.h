#pragma once
#include "ISubSystem.h"


class IGameStates;

class GameStatesMachine : public ISubSystem
{
	// Friend classing engine so that Engine class can reach the private 
	// setstate method to set our initial state.
	friend class Engine;
public:
	GameStatesMachine();
	~GameStatesMachine();

	void PushState(IGameStates* p_pxState);
	void ChangeState(IGameStates* p_pxState);
	void PopState();

	IGameStates* GetFirstGameStatePointer();
	IGameStates* GetLastGameStatePointer();
	IGameStates* GetGameStatePointerAt(int p_position);
	void PrintGameStatesListInformation();

	int Initialize();
	int Shutdown();

	bool Update();
	void Draw();

private:
	std::vector<IGameStates*> m_GameStatesListPointers;
	int m_iLastTick;
};
