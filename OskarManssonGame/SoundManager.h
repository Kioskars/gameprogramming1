#pragma once
#include "ISubSystem.h"

class Music;
class Sound;

class SoundManager : public ISubSystem
{
public:
	SoundManager();
	~SoundManager();

	int Initialize();
	int Shutdown();

	Sound* CreateSound(const std::string& p_sFilepath);
	Music* CreateMusic(const std::string& p_sFilepath);

	
	void DestroySound(Sound* p_pxSound);
	void DestroyMusic(Music* p_pxMusic);

	void PrintAllSoundsInfo();

private:
	std::map<std::string, Sound*> m_apxSoundChunks;
	std::vector<Sound*> m_apxSoundClips;
	std::map<std::string, Music*> m_apxMusicClips;
	Music* m_pxMusic;
	Sound* m_pxSound;
};