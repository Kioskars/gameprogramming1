#pragma once
#include "ISubSystem.h"

class CollisionManager;

class PhysicsCore : public ISubSystem
{
public:
	PhysicsCore();
	~PhysicsCore();

	int Initialize();
	int Shutdown();

	CollisionManager* GetCollisionManager();
private:
	CollisionManager* m_pxCollisionManager;
};