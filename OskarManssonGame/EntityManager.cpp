#include "stdafx.h"
#include "EntityManager.h"
#include "Car.h"
#include "Froggy.h"
#include "SpriteManager.h"

EntityManager::EntityManager(SpriteManager* p_pxSpriteManager)
{
	m_pxSpriteManager = p_pxSpriteManager;
}

EntityManager::EntityManager()
{
}

EntityManager::~EntityManager()
{
}

int EntityManager::Initialize()
{
	return 1;
}

int EntityManager::Shutdown()
{
	m_pxSpriteManager = nullptr;
	return 1;
}

Froggy * EntityManager::CreateFroggy()
{
	Froggy* xFroggy = new Froggy(m_xSystem.m_pxKeyboard, m_xSystem.m_pxSpriteManager->CreateSprite("../Assets/frogger.bmp", 0,0, 12, 13), 
		m_xSystem.m_iScreenWidth/2 - 12, m_xSystem.m_iScreenHeight - 100, 5000, 3, m_xSystem.m_fGfxScale, m_xSystem.m_iScreenWidth, m_xSystem.m_iScreenHeight);
	m_pxEntitiesList.push_back(xFroggy);
	return xFroggy;
}

Car * EntityManager::CreateCar()
{
	Car* xCar = new Car(m_xSystem.m_pxKeyboard, m_xSystem.m_pxSpriteManager->CreateSprite("../Assets/frogger.bmp", 0, 319, 12, 14),
		0, 0, 700, m_xSystem.m_fGfxScale, m_xSystem.m_iScreenWidth, m_xSystem.m_iScreenHeight);
	m_pxEntitiesList.push_back(xCar);
	return xCar;
}

void EntityManager::AssignSystems(System p_xSystems)
{
	m_xSystem = p_xSystems;
}

void EntityManager::DeallocCarsList()
{
	
	for (auto it = m_pxCarsList.begin(); it != m_pxCarsList.end(); ++it)
	{
		printf("Deleting sprite...");
		m_pxSpriteManager->DestroySprite((*it)->GetSprite());
		delete (*it);
		(*it) = nullptr;
	}
	m_pxCarsList.clear();
	if (m_pxCarsList.empty())
		printf("Cars list is empty!\n");
}

void EntityManager::DeallocFroggyList()
{

	for (auto it = m_pxFroggysList.begin(); it != m_pxFroggysList.end(); ++it)
	{
		printf("Deleting sprite...");
		m_pxSpriteManager->DestroySprite((*it)->GetSprite());
		delete (*it);
		(*it) = nullptr;
	}
	m_pxFroggysList.clear();
	if (m_pxFroggysList.empty())
		printf("Froggys list is empty!\n");
}
void EntityManager::DeallocEntitiesList()
{

	for (auto it = m_pxEntitiesList.begin(); it != m_pxEntitiesList.end(); ++it)
	{
		printf("Deleting sprite...");
		m_pxSpriteManager->DestroySprite((*it)->GetSprite());
		delete (*it)->GetCollider();
		delete (*it);
		(*it) = nullptr;
	}
	m_pxEntitiesList.clear();
	if (m_pxEntitiesList.empty())
		printf("Entities list is empty!\n");
}


