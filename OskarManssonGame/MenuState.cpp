#include "stdafx.h"
#include "MenuState.h"
#include "GameStatesMachine.h"
#include "SoundManager.h"
#include "Sound.h"
#include "Music.h"
#include "SpriteManager.h"
#include "GraphicsCore.h"
#include "Sprite.h"

MenuState::MenuState(System& p_xSystem)
{
	SetGameStateID(MENU_STATE);
	m_xSystem = p_xSystem;
	m_pxGameTheme = nullptr;
	m_pxMainMenu = nullptr;
}

MenuState::~MenuState()
{
	OnExit();
}

bool MenuState::Update(float p_fDeltaTime)
{
	return true;
}

void MenuState::Draw()
{
	m_xSystem.m_pxGraphicsCore->Draw(m_pxMainMenu, m_xSystem.m_iScreenWidth / 2 - m_pxMainMenu->GetRegion()->w, m_xSystem.m_iScreenHeight / 2 - m_pxMainMenu->GetRegion()->h, 2);
}

/*
* Sets up the MenuState.
*/
bool MenuState::OnEnter()
{
	printf("Entering state: %i\n", (int)GetGameStateID());
	m_pxGameTheme = m_xSystem.m_pxSoundManager->CreateMusic("../Assets/ImGay.mp3");
	m_xSystem.m_pxSoundManager->PrintAllSoundsInfo();
	m_pxGameTheme->PlayMusic();
	m_pxMainMenu = m_xSystem.m_pxSpriteManager->CreateSprite("../Assets/frogger.bmp", 0, 14, 230, 263);
	return true;
}

/*
* Deallocates everything from the MenuState.
*/
bool MenuState::OnExit()
{
	printf("Exiting state: %i\n", (int)GetGameStateID());	
	m_xSystem.m_pxSpriteManager->DestroySprite(m_pxMainMenu);
	m_pxMainMenu = nullptr;
	m_xSystem.m_bFroggyWin = false;
	return true;
}

System & MenuState::GetSystem()
{
	return m_xSystem;
}
