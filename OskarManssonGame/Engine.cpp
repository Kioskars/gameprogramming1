#include "stdafx.h"
#include "Engine.h"

#include "ISubSystem.h"
#include "GraphicsCore.h"
#include "InputManager.h"
#include "PhysicsCore.h"
#include "GameStatesMachine.h"
#include "IDSystem.h"
#include "IGameStates.h"
#include "PlayState.h"
#include "MenuState.h"
#include "EntityManager.h"
#include "SoundManager.h"

const int SCREENWIDTH = 640;
const int SCREENHEIGHT = 720;
const float GFXSCALE = 4;

Engine::Engine()
{
	m_bIsRunning = false;

	//Creates the IDsystem that keeps track of all the SubSystems.
	m_pxIDSystem = new IDSystem();

	m_pxSsGraphicsEngine = nullptr;
	m_pxSsInputManager = nullptr;	
	m_pxSsGameStatesMachine = nullptr;
	m_pxSsPhysicsCore = nullptr;
	m_pxSsEntityManager = nullptr;
	m_pxSsSoundManager = nullptr;
}

Engine::~Engine()
{
}

int Engine::Initialize()
{
	if (SDL_Init(SDL_INIT_EVERYTHING) > 0)
	{
		SDL_GetError();
		return 0;
	}

	//Initialize a graphics engine and given it an ID. Then insert this Subsystem into the IDsystems list.
	m_pxSsGraphicsEngine = new GraphicsCore();
	m_pxSsGraphicsEngine->SetSubSystemIDName("GraphicsEngine");
	
	if (m_pxSsGraphicsEngine->Initialize(SCREENWIDTH, SCREENHEIGHT) == 1)
	{
		m_pxIDSystem->InsertSubsystemWithoutID(m_pxSsGraphicsEngine);
	}
	else
	{
		//Something went wrong.
		return 0;
	}

	m_pxSsGameStatesMachine = new GameStatesMachine();
	m_pxSsGameStatesMachine->SetSubSystemIDName("GameStatesMachine");

	//I set the ID to 4, last pos, because gamestates depends on other Subsystems
	m_pxSsGameStatesMachine->SetID(4);
	if (m_pxSsGameStatesMachine->Initialize() == 1)
	{
		m_pxIDSystem->InsertSubSystemWithID(m_pxSsGameStatesMachine);
	}
	else
	{
		//Something went wrong.
		return 0;
	}

	m_pxSsInputManager = new InputManager(this, m_pxSsGameStatesMachine);
	m_pxSsInputManager->SetSubSystemIDName("InputManager");
	if (m_pxSsInputManager->Initialize() == 1)
	{
		m_pxIDSystem->InsertSubsystemWithoutID(m_pxSsInputManager);
	}
	else
	{
		//Something went wrong.
		return 0;
	}

	m_pxSsPhysicsCore = new PhysicsCore();
	m_pxSsPhysicsCore->SetSubSystemIDName("PhysicsEngine");
	if (m_pxSsPhysicsCore->Initialize() == 1)
	{
		m_pxIDSystem->InsertSubsystemWithoutID(m_pxSsPhysicsCore);
	}
	else
	{
		//Something went wrong.
		return 0;
	}

	m_pxSsEntityManager = new EntityManager(m_pxSsGraphicsEngine->GetSpriteManager());
	m_pxSsEntityManager->SetSubSystemIDName("EntityManager");
	if (m_pxSsEntityManager->Initialize() == 1)
	{
		m_pxIDSystem->InsertSubsystemWithoutID(m_pxSsEntityManager);
	}
	else
	{
		//Something went wrong
		return 0;
	}

	m_pxSsSoundManager = new SoundManager();
	m_pxSsSoundManager->SetSubSystemIDName("SoundManager");
	if (m_pxSsSoundManager->Initialize() == 1)
	{
		m_pxIDSystem->InsertSubsystemWithoutID(m_pxSsSoundManager);
	}
	else
	{
		//Something went wrong
		return 0;
	}
	m_pxIDSystem->PrintListSubSystemsInformation();

	

	System system;
	system.m_iScreenHeight = SCREENHEIGHT;
	system.m_iScreenWidth = SCREENWIDTH;
	system.m_fGfxScale = GFXSCALE;
	system.m_pxGraphicsCore = m_pxSsGraphicsEngine;
	system.m_pxKeyboard = m_pxSsInputManager->GetKeyboard();
	system.m_pxSpriteManager = m_pxSsGraphicsEngine->GetSpriteManager();
	system.m_pxCollisionManager = m_pxSsPhysicsCore->GetCollisionManager();
	system.m_pxGameStatesMachine = m_pxSsGameStatesMachine;
	system.m_pxEntityManager = m_pxSsEntityManager;
	system.m_pxSoundManager = m_pxSsSoundManager;


	m_pxSsGameStatesMachine->ChangeState(new MenuState(system));
	m_pxSsEntityManager->AssignSystems(system);

	m_bIsRunning = true;

	return 1;
}

void Engine::Update()
{
	while (m_bIsRunning)
	{
		m_pxSsInputManager->HandleEvents();
		m_pxSsGraphicsEngine->Clear();
		if (m_pxSsGameStatesMachine->Update() == false)
		{
			m_bIsRunning = false;
		}
		m_pxSsGameStatesMachine->Draw();
		m_pxSsGraphicsEngine->Present();
		SDL_Delay(10);
	}	
}
/*
* Checks if there are any subsystems running. If there are it will call the Dealloc function. 
*/
int Engine::Shutdown()
{
	printf("Checking if ListSubSystem is empty...\n");
	bool bEmptyList = m_pxIDSystem->m_ListSubSystems.empty();
	m_pxIDSystem->PrintListSubSystemsInformation();
	if (bEmptyList == true)
	{
		printf("ListSubSystem is empty....Safe shutdown\n");
		delete m_pxIDSystem;
		m_pxIDSystem = nullptr;
		SDL_Quit();
		return 0;
	}
	else
	{
		for (auto it = m_pxIDSystem->m_ListSubSystems.rbegin(); it != m_pxIDSystem->m_ListSubSystems.rend(); ++it)
		{
			DeallocSubSystem(it->second);
		}
		m_pxIDSystem->m_ListSubSystems.clear();
	}
	Shutdown();
}

void Engine::SetRunning(bool p_bRunning)
{
	m_bIsRunning = p_bRunning;
}

SDL_Renderer * Engine::GetEngineRenderer()
{
	return m_pxSsGraphicsEngine->GetRenderer();
}

/*
* Calls the parameters subsystems shutdown function. Then delete it and resets the pointer to nullptr.
*/
void Engine::DeallocSubSystem(ISubSystem* p_pxSubSystem)
{	
	printf("Deallocating SubSystem....%i %s %p \n", p_pxSubSystem->GetID(), p_pxSubSystem->GetSubSystemIDName().c_str(), p_pxSubSystem);
	if (p_pxSubSystem->Shutdown() <= 0)
	{
		printf("Something happened, can't deallocate");
		return;
	}
	delete p_pxSubSystem;
	p_pxSubSystem = nullptr;
}
