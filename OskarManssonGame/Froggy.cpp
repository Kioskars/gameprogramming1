#include "stdafx.h"
#include "Froggy.h"
#include "Keyboard.h"
#include "Sprite.h"
#include "Collider.h"

Froggy::Froggy(Keyboard * p_pxKeyboard, Sprite * p_pxSprite, float p_fX, float p_fY, float p_fSpeed, int p_iLives, float p_fGfxScale, int p_iScreenWidth, int p_iScreenHeight)
{
	m_pxKeyboard = p_pxKeyboard;
	m_pxSprite = p_pxSprite;
	m_fX = p_fX;
	m_fY = p_fY;
	m_fSpeed = p_fSpeed;
	m_iLives = p_iLives;
	m_fGfxScale = p_fGfxScale;
	m_iScreenWidth = p_iScreenWidth;
	m_iScreenHeight = p_iScreenHeight;
	m_bVisible = true;
	m_pxCollider = new Collider(
		m_pxSprite->GetRegion()->w*p_fGfxScale,
		m_pxSprite->GetRegion()->h*p_fGfxScale);
	m_pxCollider->SetParent(this);
}

Froggy::~Froggy()
{
	delete m_pxKeyboard;
	m_pxKeyboard = nullptr;

	delete m_pxCollider;
	m_pxCollider = nullptr;
}

void Froggy::Update(float p_fDeltaTime)
{
	Movement(p_fDeltaTime);
	m_pxCollider->Refresh();
}

Sprite * Froggy::GetSprite()
{
	return m_pxSprite;
}

Collider * Froggy::GetCollider()
{
	return m_pxCollider;
}

float Froggy::GetX()
{
	return m_fX;
}

float Froggy::GetY()
{
	return m_fY;
}

void Froggy::SetX(float p_fX)
{
	m_fX = p_fX;
}

void Froggy::SetY(float p_fY)
{
	m_fY = p_fY;
}

int Froggy::GetLives()
{
	return m_iLives;
}

bool Froggy::IsVisible()
{
	return m_bVisible;
}

//Checks what key gets pushed down then updates the froggers position.
void Froggy::Movement(float p_fDeltaTime)
{
	
	int xCurSpeed = m_fSpeed * p_fDeltaTime;
	if (m_pxKeyboard->IsKeyDown(SDLK_w))
	{
		if (m_fY > 0)
			m_fY -= xCurSpeed;
		m_pxKeyboard->SetKey(SDLK_w, false);
	}
	if (m_pxKeyboard->IsKeyDown(SDLK_s))
	{
		if (m_fY < m_iScreenHeight - m_pxCollider->GetH() - 50)
			m_fY += xCurSpeed;
		if (m_fY >= m_iScreenHeight - m_pxCollider->GetH() - 50)
			m_fY = m_iScreenHeight - m_pxCollider->GetH() - 50;

		m_pxKeyboard->SetKey(SDLK_s, false);
	}
	if (m_pxKeyboard->IsKeyDown(SDLK_a))
	{		
		if (m_fX > 0)
			m_fX -= xCurSpeed;

		if (m_fX <= 0)
			m_fX = 0;

		m_pxKeyboard->SetKey(SDLK_a, false);
	}
	if (m_pxKeyboard->IsKeyDown(SDLK_d))
	{		
		if(m_fX < m_iScreenWidth - m_pxCollider->GetW())
			m_fX += xCurSpeed;
		if (m_fX >= m_iScreenWidth - m_pxCollider->GetW())
			m_fX = m_iScreenWidth - m_pxCollider->GetW();

		m_pxKeyboard->SetKey(SDLK_d, false);
	}
}

void Froggy::LoseLife()
{
	--m_iLives;
	printf("Froggy lost a life\n");
	printf("Nr of lives left: %i\n", m_iLives);
}

EENTITYTYPE Froggy::GetType()
{
	return EENTITYTYPE::ENTITY_FROGGY;
}
