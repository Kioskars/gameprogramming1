#include "stdafx.h"
#include "IDSystem.h"

#include "ISubSystem.h"

std::map<int, ISubSystem*> m_ListSubSystems;

IDSystem::IDSystem()
{
	m_SubSystemID = NULL;
}

void IDSystem::InsertSubSystemWithID(ISubSystem* p_pxSubSystem)
{
	//check if Subsystem already exist in list
	for (auto it = m_ListSubSystems.begin(); it != m_ListSubSystems.end();)
	{
		std::cout << "Searching for SubSystem, " << p_pxSubSystem->GetSubSystemIDName() << " in  m_list at pos " << it->first << std::endl;
		if (it->second == p_pxSubSystem)
		{
			m_ListSubSystems.erase(it++);
			printf("Found and erased old SubSystem\n");
		}
		else
		{
			++it;
		}
	}

	if (p_pxSubSystem->GetID() == NULL)
	{
		printf("Error! Subsystem has no ID\n");
		return;
	}

	m_ListSubSystems.insert(std::pair <int, ISubSystem*>(p_pxSubSystem->GetID(), p_pxSubSystem));
	std::cout << "Inserted SubSystem: " << p_pxSubSystem->GetSubSystemIDName() << ". At position: " << p_pxSubSystem->GetID() << std::endl;
}

void IDSystem::InsertSubsystemWithoutID(ISubSystem* p_pxSubSystem)
{
	//check if Subsystem exist already exist in list
	for (auto it = m_ListSubSystems.begin(); it != m_ListSubSystems.end();)
	{
		std::cout << "Searching for SubSystem, " << p_pxSubSystem->GetSubSystemIDName() << " in  m_list" << std::endl;

		if (it->second == p_pxSubSystem)
		{
			m_ListSubSystems.erase(it++);
			printf("Found and erased old SubSystem\n");
		}
		else
		{
			++it;
		}

	}

	if (p_pxSubSystem->GetID() == NULL && m_SubSystemID == NULL)
	{
		m_SubSystemID = 0;
		p_pxSubSystem->SetID(m_SubSystemID);
		std::cout << p_pxSubSystem->GetSubSystemIDName() << " was assigned ID: " << m_SubSystemID << std::endl;
		m_ListSubSystems.insert(std::pair <int, ISubSystem*>(p_pxSubSystem->GetID(), p_pxSubSystem));
		std::cout << "Inserted SubSystem: " << p_pxSubSystem->GetSubSystemIDName() << ". At position: " << p_pxSubSystem->GetID() << std::endl;
		++m_SubSystemID;
	}
	else
	{
		p_pxSubSystem->SetID(m_SubSystemID);
		std::cout << p_pxSubSystem->GetSubSystemIDName() << " was assigned ID: " << m_SubSystemID << std::endl;
		m_ListSubSystems.insert(std::pair <int, ISubSystem*>(p_pxSubSystem->GetID(), p_pxSubSystem));
		std::cout << "Inserted SubSystem: " << p_pxSubSystem->GetSubSystemIDName() << ". At position: " << p_pxSubSystem->GetID() << std::endl;
		++m_SubSystemID;
	}
}

ISubSystem * IDSystem::GetSubSystemWithID(int p_iSubSystemID)
{
	if (m_ListSubSystems.empty() == true)
	{
		printf("Subsystem map is Empty.\n");
		return nullptr;
	}
	
	auto it = m_ListSubSystems.begin();

	for (int i = 0; i != p_iSubSystemID; ++i)
	{
		if (it->first == p_iSubSystemID)
		{
			printf("Found %s%s", it->second->GetSubSystemIDName().c_str(), ". Returning its pointer...\n");
			return it->second;
		}
		++it;
	}
		printf("Could not find Subsystem with that ID\n");
		return nullptr;		
}

ISubSystem * IDSystem::GetSubSystemWithIDName(std::string p_sSubSystemNameID)
{
	if (m_ListSubSystems.empty() == true)
	{
		printf("Subsystem map is Empty\n");
		return nullptr;
	}

	auto it = m_ListSubSystems.begin();

	for (int i = 0; i <= m_ListSubSystems.end()->first; ++i)
	{
		if (it->second->GetSubSystemIDName().c_str() == p_sSubSystemNameID)
		{
			printf("Found %s %s", it->second->GetSubSystemIDName().c_str(), ". Returning its pointer...\n");
			return it->second;
		}
		++it;
	}
	printf("Could not find Subsystem with that name ID\n");
	return nullptr;
}

void IDSystem::PrintListSubSystemsInformation()
{
	std::cout << "SubSystem ID";
	std::cout << std::setw(21) << "SubSystem Name";
	std::cout << std::setw(21) << "Subsystem Adress" << std::endl;
	for (auto it = m_ListSubSystems.begin(); it != m_ListSubSystems.end(); ++it)
	{
		std::cout << it->first;
		std::cout << std::setw(30) << it->second->GetSubSystemIDName();
		std::cout << std::setw(21) << it->second << std::endl;
	}
}
