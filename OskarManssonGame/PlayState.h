#pragma once
#include "IGameStates.h"

class Froggy;
class Car;
class CollisionManager;
class EntityManager;
class Music;
class Sound;
class Sprite;
class IEntity;

class PlayState : public IGameStates
{
public:
	PlayState(System& p_xSystem);
	~PlayState();

	bool Update(float p_fDeltaTime);
	void Draw();

	bool OnEnter();
	bool OnExit();

	System& GetSystem();
	void FroggyCollidedAndLostALife(Car* p_xCar);
	bool FroggyWin(Froggy* p_xFroggy);

private:
	System m_xSystem;
	Froggy* m_xFroggy;
	Car* m_xCar;
	Car* m_xCar2;
	Car* m_xCar3;
	Car* m_xCar4;
	Sound* m_pxFroggyCollideSound;

	std::vector<IEntity*> m_apxGameObjects;
	
	CollisionManager* m_pxCollisionManager;
	EntityManager* m_pxEntityManager;
};