#include "stdafx.h"
#include "GraphicsCore.h"
#include "SpriteManager.h"
#include "Sprite.h"
#include "HUDmanager.h"

GraphicsCore::GraphicsCore()
{
	m_pxWindow = nullptr;
	m_pxRenderer = nullptr;
	m_pxSpriteManager = nullptr;
	m_pxHUDManager = nullptr;
}

GraphicsCore::~GraphicsCore()
{
}

int GraphicsCore::Initialize()
{
	return 1;
}
/*
* Creates a window with a renderer, a SpriteManager and a HUDmanager.
*/
int GraphicsCore::Initialize(int p_SCREENWIDTH, int p_SCREENHEIGHT)
{
	if ((m_pxWindow = SDL_CreateWindow("Title", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
		p_SCREENWIDTH, p_SCREENHEIGHT, SDL_WINDOW_OPENGL)) < 0)
	{
		SDL_GetError();
		return 0;
	}
	
	if ((m_pxRenderer = SDL_CreateRenderer(m_pxWindow, -1, 0)) < 0)
	{
		SDL_GetError();
		return 0;
	}
	
	SDL_SetRenderDrawColor(m_pxRenderer,
		0x00, 0x00, 0x00, 0xff);

	m_pxSpriteManager = new SpriteManager(m_pxRenderer);

	m_pxHUDManager = new HUDmanager(this, m_pxSpriteManager);
	return 1;
}
/*
* Deallocates all of the GraphicsCores subsystems.
*/
int GraphicsCore::Shutdown()
{

	delete m_pxHUDManager;
	m_pxHUDManager = nullptr;

	delete m_pxSpriteManager;
	m_pxSpriteManager = nullptr;

	SDL_DestroyRenderer(m_pxRenderer);
	m_pxRenderer = nullptr;

	SDL_DestroyWindow(m_pxWindow);
	m_pxWindow = nullptr;

	return 1;
}

SpriteManager * GraphicsCore::GetSpriteManager()
{
	return m_pxSpriteManager;
}

SDL_Renderer * GraphicsCore::GetRenderer()
{
	return m_pxRenderer;
}

HUDmanager * GraphicsCore::GetHUDmanager()
{
	return m_pxHUDManager;
}

void GraphicsCore::Draw(Sprite* p_pxSprite, int p_iX, int p_iY, float p_fScale)
{
	// Creates a destination rect by combining the position parameters with the Sprite region data.
	// This is later used to know where and how big will
	// render thic picture.
	rect = { p_iX, p_iY,
	p_pxSprite->GetRegion()->w * (int)p_fScale, p_pxSprite->GetRegion()->h * (int)p_fScale };


	// RenderCopy renders a source rectangle from a texture to a target rectangle on a renderer. Then we render the piece the Sprites Region specifies
	// of the Sprites SDL_Texture to the correct destination we created earlier.
	SDL_RenderCopy(m_pxRenderer, p_pxSprite->GetTexture(),
		p_pxSprite->GetRegion(), &rect);
}

void GraphicsCore::Clear()
{
	SDL_RenderClear(m_pxRenderer);
}

void GraphicsCore::Present()
{
	SDL_RenderPresent(m_pxRenderer);
}

