#include "stdafx.h"
#include "GameStatesMachine.h"
#include "IGameStates.h"

GameStatesMachine::GameStatesMachine()
{
	m_iLastTick = SDL_GetTicks();
}

GameStatesMachine::~GameStatesMachine()
{
}

/*
* Pushes the parameters state into the vector container m_GameStatesListPointers. Then Enters that state.
*/
void GameStatesMachine::PushState(IGameStates * p_pxState)
{
	m_GameStatesListPointers.push_back(p_pxState);
	m_GameStatesListPointers.back()->OnEnter();
}
/*
 Checks if we want to go into the current state (do nothing) or go to another state.

*/
void GameStatesMachine::ChangeState(IGameStates * p_pxState)
{
	if (!m_GameStatesListPointers.empty())
	{
		if (m_GameStatesListPointers.back()->GetGameStateID() == p_pxState->GetGameStateID())
		{
			return; //do nothing
		}
		if (m_GameStatesListPointers.front()->OnExit())
		{
			//Deallocate the old state
			PopState();
		}
	}

	PushState(p_pxState);
}

/*
* Deallocates the state
*/
void GameStatesMachine::PopState()
{
	if (!m_GameStatesListPointers.empty())
	{
		if (m_GameStatesListPointers.back()->OnExit())
		{
			delete m_GameStatesListPointers.back();
			m_GameStatesListPointers.back() = nullptr;
			m_GameStatesListPointers.pop_back();
		}
	}
}

IGameStates * GameStatesMachine::GetFirstGameStatePointer()
{
	IGameStates* xReturningPointer = m_GameStatesListPointers.front();
	return xReturningPointer;
}

IGameStates * GameStatesMachine::GetLastGameStatePointer()
{
	IGameStates* xReturningPointer = m_GameStatesListPointers.back();
	return xReturningPointer;
}

IGameStates * GameStatesMachine::GetGameStatePointerAt(int p_position)
{
	IGameStates* xReturningPointer = m_GameStatesListPointers.at(p_position);
	return xReturningPointer;
}

void GameStatesMachine::PrintGameStatesListInformation()
{
	printf("GameStateID\tGameStateMemoryAdress\n");
	for (auto it = m_GameStatesListPointers.begin(); it != m_GameStatesListPointers.end(); ++it)
	{
		printf("%i\t\t%p\n", (*it)->GetGameStateID(), (*it));
	}
}

int GameStatesMachine::Initialize()
{
	return 1;
}

int GameStatesMachine::Shutdown()
{
	PrintGameStatesListInformation();
	PopState();
	m_GameStatesListPointers.clear();
	
	return 1;
}

bool GameStatesMachine::Update()
{
	
	float fDeltaTime = (SDL_GetTicks() - m_iLastTick) * 0.001f;

	if (!m_GameStatesListPointers.empty())
	{
		m_iLastTick = SDL_GetTicks();
		return m_GameStatesListPointers.back()->Update(fDeltaTime);
	}
	return false;
}

void GameStatesMachine::Draw()
{
	if (!m_GameStatesListPointers.empty())
	{
		m_GameStatesListPointers.back()->Draw();
		//m_pxCurrentGameState->Draw();
	}
}
