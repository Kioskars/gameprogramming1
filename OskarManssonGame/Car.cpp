#include "stdafx.h"
#include "Car.h"
#include "Keyboard.h"
#include "Sprite.h"
#include "Collider.h"

Car::Car(Keyboard* p_pxKeyboard, Sprite* p_pxSprite,
	float p_fX, float p_fY, float p_fSpeed, float p_fGfxScale,
	int p_iScreenWidth, int p_iScreenHeight)
{
	m_pxKeyboard = p_pxKeyboard;
	m_pxSprite = p_pxSprite;
	m_fX = p_fX;
	m_fY = p_fY;
	m_fSpeed = p_fSpeed;
	m_fGfxScale = p_fGfxScale;
	m_iScreenWidth = p_iScreenWidth;
	m_iScreenHeight = p_iScreenHeight;
	m_bVisible = true;
	m_pxCollider = new Collider(
		m_pxSprite->GetRegion()->w*p_fGfxScale,
		m_pxSprite->GetRegion()->h*p_fGfxScale);
	m_pxCollider->SetParent(this);
}

Car::~Car()
{
	delete m_pxKeyboard;
	m_pxKeyboard = nullptr;
	delete m_pxCollider;
	m_pxCollider = nullptr;
}

void Car::Update(float p_fDeltaTime)
{
	Movement(p_fDeltaTime);
	m_pxCollider->Refresh();
}

void Car::Draw(Sprite * p_pxSprite, int p_iX, int p_iY, float p_fScale)
{
}

Sprite * Car::GetSprite()
{
	return m_pxSprite;
}

Collider * Car::GetCollider()
{
	return m_pxCollider;
}

float Car::GetX()
{
	return m_fX;
}

float Car::GetY()
{
	return m_fY;
}

void Car::SetXandY(float p_fX, float p_fY)
{
	m_fX = p_fX;
	m_fY = p_fY;
}

void Car::SetSpeed(float p_fSpeed)
{
	m_fSpeed = p_fSpeed;
}

void Car::SetGfxScale(float p_fGfxScale)
{
	m_fGfxScale = p_fGfxScale;
}

bool Car::IsVisible()
{
	return true;
}

void Car::Movement(float p_fDeltaTime)
{
	int xCurSpeed = m_fSpeed*p_fDeltaTime;
	m_fX += xCurSpeed;
	if (m_fX >= (m_iScreenWidth + 50))
		m_fX = -150;
}

EENTITYTYPE Car::GetType()
{
	return EENTITYTYPE::ENTITY_CAR;
}
