#pragma once

#include "stdafx.h"
#include "ISubSystem.h"

class SpriteManager;
class Sprite;
class HUDmanager;

class GraphicsCore : public ISubSystem
{
public:
	GraphicsCore();
	~GraphicsCore();

	int Initialize();
	int Initialize(int p_SCREENWIDTH, int p_SCREENHEIGHT);
	int Shutdown();

	SpriteManager* GetSpriteManager();
	SDL_Renderer* GetRenderer();
	HUDmanager* GetHUDmanager();

	void Draw(Sprite* p_pxSprite, int p_iX, int p_iY,  float p_fScale);
	void Clear();
	void Present();

private:
	SDL_Window* m_pxWindow;
	SDL_Renderer* m_pxRenderer;
	SpriteManager* m_pxSpriteManager;
	HUDmanager* m_pxHUDManager;
	SDL_Rect rect;
};