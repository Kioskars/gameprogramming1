#include "stdafx.h"
#include "HUDmanager.h"
#include "Sprite.h"
#include "SpriteManager.h"
#include "GraphicsCore.h"

HUDmanager::HUDmanager(GraphicsCore* p_pxGraphicsCore, SpriteManager* p_pxSpriteManager)
{
	m_pxSpriteManager = p_pxSpriteManager;
	m_pxGraphicsCore = p_pxGraphicsCore;
	m_iTime = 152;
	xIteration = 0;
	m_xLife = nullptr;
	xGoal = nullptr;
	xControls = nullptr;
	
}

HUDmanager::~HUDmanager()
{
	if (!(m_xLife == nullptr))
	{
		m_pxSpriteManager->DestroySprite(m_xLife);
		m_xLife = nullptr;
	}
		
	if (!(xControls == nullptr))
	{
		m_pxSpriteManager->DestroySprite(xControls);
		xControls = nullptr;
	}

	if (!(xGoal == nullptr))
	{
		m_pxSpriteManager->DestroySprite(xGoal);
		xGoal = nullptr;
	}
}


void HUDmanager::CreatePlayerLives(int p_iLives, int p_iX, int p_iY)
{
	m_xLife = m_pxSpriteManager->CreateSprite("../Assets/frogger.bmp", 0, 0, 12, 13);
	if (m_apxPlayerLivesHUD.size() <= p_iLives)
	{
		m_iPosX = p_iX;
		m_iPosY = p_iY;

		for (int i = 0; i < p_iLives; i++)
		{
			m_apxPlayerLivesHUD.push_back(m_xLife);
		}
	}
}

void HUDmanager::TimeCounter(int p_iX, int p_iY)
{
	xTime = m_pxSpriteManager->CreateSprite("../Assets/frogger.bmp", 13, 0, m_iTime, 8);
	m_pxGraphicsCore->Draw(xTime, p_iX, p_iY, 2);
	m_pxSpriteManager->DestroySprite(xTime);
	xTime = nullptr;
}

//Chips away one pixel from our TimeCounter image.
void HUDmanager::UpdateTimeCounter(float p_Time)
{
	xIteration += p_Time;
	if (xIteration >= .1)
	{
		--m_iTime;
		xIteration = 0;
	}
	
}

void HUDmanager::SetTimeLeft(int p_xTime)
{
	m_iTime = p_xTime;
}

int HUDmanager::GetTimeleft()
{
	return m_iTime;
}

void HUDmanager::DrawGameInstructions()
{
	m_pxGraphicsCore->Draw(xControls, 20, 620, 2);
	m_pxGraphicsCore->Draw(xGoal, 320-xGoal->GetRegion()->w, 0, 2);
}

void HUDmanager::CreateGameInstructions()
{
	xControls = m_pxSpriteManager->CreateSprite("../Assets/frogger.bmp", 230, 115, 53, 41);
	xGoal = m_pxSpriteManager->CreateSprite("../Assets/frogger.bmp", 244, 16, 102, 70);
}

void HUDmanager::DeAllocGameInstructions()
{
	m_pxSpriteManager->DestroySprite(xControls);
	m_pxSpriteManager->DestroySprite(xGoal);

	xControls = nullptr;
	xGoal = nullptr;
}

void HUDmanager::DrawPlayerLives(int p_iLives, int p_iX, int p_iY)
{
	int LifePosX = p_iX;
	int LifePosY = p_iY;

	for (auto it = m_apxPlayerLivesHUD.rbegin(); it != m_apxPlayerLivesHUD.rend(); ++it)
	{
		m_pxGraphicsCore->Draw((*it), LifePosX, LifePosY, 2);
		LifePosX -= 35;
	}
}

void HUDmanager::DestroyLife()
{
	if (!m_apxPlayerLivesHUD.empty())
	{
		m_apxPlayerLivesHUD.back() = nullptr;
		m_apxPlayerLivesHUD.pop_back();
	}
}

void HUDmanager::DeAllocLives()
{
	if (!m_apxPlayerLivesHUD.empty())
		m_apxPlayerLivesHUD.clear();

	m_pxSpriteManager->DestroySprite(m_xLife);
}
