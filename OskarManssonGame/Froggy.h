#pragma once
#include "IEntity.h"
class Keyboard;

class Froggy : public IEntity
{
public:
	Froggy(Keyboard* p_pxKeyboard, Sprite* p_pxSprite,
		float p_fX, float p_fY, float p_fSpeed, int p_iLives, float p_fGfxScale,
		int p_iScreenWidth, int p_iScreenHeight);
	Froggy() {};
	~Froggy();
	void Update(float p_fDeltaTime);
	Sprite* GetSprite();
	Collider* GetCollider();
	float GetX();
	float GetY();
	void SetX(float p_fX);
	void SetY(float p_fY);
	int GetLives();
	bool IsVisible();
	void Movement(float p_fDeltaTime);
	void LoseLife();
	EENTITYTYPE GetType();
private:
	
	Sprite* m_pxSprite;
	Collider* m_pxCollider;
	Keyboard* m_pxKeyboard;
	float m_fX;
	float m_fY;
	float m_fSpeed;
	int m_iLives;
	float m_fGfxScale;
	int m_iScreenWidth;
	int m_iScreenHeight;
	bool m_bVisible;
};