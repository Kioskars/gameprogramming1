// OskarManssonGame.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Engine.h"


int main(int argc, char* argv[])
{
	Engine gameEngine;

	if (gameEngine.Initialize() > 0)
	{
		gameEngine.Update();
	}
	
	if (gameEngine.Shutdown() == 0)
	{
		system("PAUSE");
		return 0;
	}

	return 1;
}

