#pragma once

class IDSystem;

class ISubSystem;
class GraphicsCore;
class InputManager;
class GameStatesMachine;
class PhysicsCore;
class EntityManager;
class SoundManager;


class Engine
{
public:
	Engine();
	~Engine();

	int Initialize();
	void Update();
	int Shutdown();
	
	void SetRunning(bool p_bRunning);
	SDL_Renderer* GetEngineRenderer();
	

private:
	bool m_bIsRunning;
	void DeallocSubSystem(ISubSystem* p_pxSubSystem);
	IDSystem* m_pxIDSystem;

	GraphicsCore* m_pxSsGraphicsEngine;
	InputManager* m_pxSsInputManager;
	GameStatesMachine* m_pxSsGameStatesMachine;
	PhysicsCore* m_pxSsPhysicsCore;
	EntityManager* m_pxSsEntityManager;
	SoundManager* m_pxSsSoundManager;
};