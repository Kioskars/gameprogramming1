#pragma once

class Sprite;
class SpriteManager;
class GraphicsCore;

class HUDmanager
{
public:
	HUDmanager(GraphicsCore* p_pxGraphicsCore, SpriteManager* p_pxSpriteManager);
	~HUDmanager();

	void CreatePlayerLives(int p_iLives, int p_iX, int p_iY);
	void TimeCounter(int p_iX, int p_iY);
	void UpdateTimeCounter(float p_Time);

	void SetTimeLeft(int p_xTime);
	int GetTimeleft();

	void DrawGameInstructions();
	void CreateGameInstructions();
	void DeAllocGameInstructions();

	void DrawPlayerLives(int p_iLives, int p_iX, int p_iY);
	void DestroyLife();
	void DeAllocLives();
private:
	
	std::vector<Sprite*> m_apxPlayerLivesHUD;
	SpriteManager* m_pxSpriteManager;
	GraphicsCore* m_pxGraphicsCore;
	int m_iPosX;
	int m_iPosY;
	int m_iTime;
	float xIteration;
	Sprite* xTime;
	Sprite* m_xLife;
	Sprite* xControls;
	Sprite* xGoal;
};