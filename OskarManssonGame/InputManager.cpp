#include "stdafx.h"
#include "InputManager.h"
#include "Engine.h"
#include "Keyboard.h"
#include "Mouse.h"
#include "GameStatesMachine.h"
#include "IGameStates.h"
#include "PlayState.h"
#include "MenuState.h"
#include "EndState.h"


InputManager::InputManager(Engine* p_pxEngine, GameStatesMachine* p_pxGameStateMachine)
{
	m_pxEngine = p_pxEngine;
	m_pxMouse = nullptr;
	m_pxKeyboard = nullptr;
	m_pxGameStateMachine = p_pxGameStateMachine;
}

InputManager::~InputManager()
{
	Shutdown();
}

int InputManager::Initialize()
{
	m_pxMouse = new Mouse();
	m_pxKeyboard = new Keyboard();

	return 1;
}

int InputManager::Shutdown()
{

	delete m_pxKeyboard;
	m_pxKeyboard = nullptr;

	delete m_pxMouse;
	m_pxMouse = nullptr;

	m_pxEngine = nullptr;

	m_pxGameStateMachine = nullptr;

	return 1;
}
/*
* Check for Input event. On key down it sets the Keys boolean array of true or false to true due to it being pushed down.
* When it goes up it automatically goes to false.
*/
void InputManager::HandleEvents()
{
	SDL_Event xEvent;
	while (SDL_PollEvent(&xEvent))
	{
		if (xEvent.type == SDL_QUIT)
		{
			m_pxEngine->SetRunning(false);
		}
		else if (xEvent.type == SDL_MOUSEMOTION)
		{
			m_pxMouse->SetPosition(xEvent.motion.x, xEvent.motion.y);
		}
		else if (xEvent.type == SDL_KEYDOWN)
		{
			m_pxKeyboard->SetKey(xEvent.key.keysym.sym, true);
			switch (xEvent.key.keysym.sym)
			{
			case SDLK_ESCAPE:
				m_pxEngine->SetRunning(false);
				break;
			case SDLK_k:
				SDL_SetRenderDrawColor(m_pxEngine->GetEngineRenderer(), 255, 55, 55, 55);
				break;
			case SDLK_l:
				SDL_SetRenderDrawColor(m_pxEngine->GetEngineRenderer(), 255, 255, 55, 55);		
				break;
			case SDLK_0:
				m_pxGameStateMachine->ChangeState(new MenuState(m_pxGameStateMachine->GetLastGameStatePointer()->GetSystem()));
				break;
			case SDLK_1:
				m_pxGameStateMachine->ChangeState(new EndState(m_pxGameStateMachine->GetLastGameStatePointer()->GetSystem()));
				break;
			case SDLK_RETURN:
				if(m_pxGameStateMachine->GetLastGameStatePointer()->GetGameStateID() == MENU_STATE)
					m_pxGameStateMachine->ChangeState(new PlayState(m_pxGameStateMachine->GetLastGameStatePointer()->GetSystem()));
				if (m_pxGameStateMachine->GetLastGameStatePointer()->GetGameStateID() == END_STATE)
					m_pxGameStateMachine->ChangeState(new MenuState(m_pxGameStateMachine->GetLastGameStatePointer()->GetSystem()));
				break;
			case SDLK_p:
				m_pxGameStateMachine->PrintGameStatesListInformation();
				break;
			
			default:
				break;
			}
		}
		else if (xEvent.type == SDL_KEYUP)
		{
			m_pxKeyboard->SetKey(xEvent.key.keysym.sym, false);
			SDL_SetRenderDrawColor(m_pxEngine->GetEngineRenderer(), 0, 0, 0, 0);
		}
	}
}

Mouse * InputManager::GetMouse()
{
	return m_pxMouse;
}

Keyboard * InputManager::GetKeyboard()
{
	return m_pxKeyboard;
}

int InputManager::GetMouseX()
{
	return m_pxMouse->GetX();
}

int InputManager::GetMouseY()
{
	return m_pxMouse->GetY();
}
