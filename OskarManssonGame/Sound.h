#pragma once

class Sound
{
public:
	Sound(std::string p_sFilepath);
	~Sound();

	void PlaySound();
	void StopSound();

	std::string GetSoundName();

	Mix_Chunk* GetSoundChunk();
	void SetSoundClip(Mix_Chunk* p_pxMixChunk);
private:
	Mix_Chunk* m_pxSoundClip;
	int m_iLastChannelPlayed;
	std::string m_sSoundName;
};