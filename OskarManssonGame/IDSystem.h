#pragma once

class ISubSystem;

class IDSystem
{
public:
	IDSystem();
	std::map<int, ISubSystem*> m_ListSubSystems;

	void InsertSubSystemWithID(ISubSystem* p_pxSubSystem);
	void InsertSubsystemWithoutID(ISubSystem* p_pxSubSystem);
	ISubSystem* GetSubSystemWithID(int p_iSubSystemID);
	ISubSystem* GetSubSystemWithIDName(std::string p_sSubSystemNameID);

	void PrintListSubSystemsInformation();

private:
	int m_SubSystemID;
};