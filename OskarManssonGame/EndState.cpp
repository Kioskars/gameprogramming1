#include "stdafx.h"
#include "EndState.h"
#include "IGameStates.h"
#include "GraphicsCore.h"
#include "SpriteManager.h"
#include "Sprite.h"
#include "Music.h"
#include "SoundManager.h"

EndState::EndState(System& p_xSystem)
{	
	SetGameStateID(END_STATE);
	m_xSystem = p_xSystem;
	m_pxEndScreen = nullptr;
	m_pxNewGame = nullptr;
}

EndState::~EndState()
{
	OnExit();
}

/*
* Sets up the EndState.
*/
bool EndState::OnEnter()
{
	printf("Entering EndScreen ID: %i\n", (int)GetGameStateID());
	if (m_xSystem.m_bFroggyWin == true)
	{
		m_pxEndScreen = m_xSystem.m_pxSpriteManager->CreateSprite("../Assets/frogger.bmp", 0, 349, 154, 38);
	}
	else
	{
		m_pxEndScreen = m_xSystem.m_pxSpriteManager->CreateSprite("../Assets/frogger.bmp", 0, 395, 154, 38);
	}
	m_pxNewGame = m_xSystem.m_pxSpriteManager->CreateSprite("../Assets/frogger.bmp", 0, 433, 154, 68);
	m_xSystem.m_bFroggyWin = false;
	return true;
}

bool EndState::Update(float p_fDeltaTime)
{
	return true;
}

/*
* Deallocates everything from EndState.
*/
bool EndState::OnExit()
{
	m_xSystem.m_pxSpriteManager->DestroySprite(m_pxEndScreen);
	m_pxEndScreen = nullptr;

	m_xSystem.m_pxSpriteManager->DestroySprite(m_pxNewGame);
	m_pxNewGame = nullptr;
	
	return true;
}

void EndState::Draw()
{
	m_xSystem.m_pxGraphicsCore->Draw(m_pxEndScreen, 
		m_xSystem.m_iScreenWidth / 2 - m_pxEndScreen->GetRegion()->w, 
		m_xSystem.m_iScreenHeight / 2 - m_pxEndScreen->GetRegion()->h, 
		2);
	m_xSystem.m_pxGraphicsCore->Draw(m_pxNewGame,
		m_xSystem.m_iScreenWidth / 2 - m_pxEndScreen->GetRegion()->w + 30,
		m_xSystem.m_iScreenHeight / 2 - m_pxEndScreen->GetRegion()->h + 90,
		2);
}

System & EndState::GetSystem()
{
	return m_xSystem;
}
