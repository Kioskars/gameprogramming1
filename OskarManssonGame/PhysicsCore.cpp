#include "stdafx.h"
#include "PhysicsCore.h"
#include "CollisionManager.h"

PhysicsCore::PhysicsCore()
{
	SetSubSystemIDName("PhysicsCore");
	m_pxCollisionManager = nullptr;
}

PhysicsCore::~PhysicsCore()
{
}

int PhysicsCore::Initialize()
{
	m_pxCollisionManager = new CollisionManager();
	return 1;
}

int PhysicsCore::Shutdown()
{
	delete m_pxCollisionManager;
	m_pxCollisionManager = nullptr;
	return 1;
}

CollisionManager * PhysicsCore::GetCollisionManager()
{
	return m_pxCollisionManager;
}
